"use strict";

// Provided variables.
var year = 1988;

// Variables you'll be assigning to in this question.
var isLeapYear;

// TODO Your code for part (2) here.
 isLeapYear = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);

// Printing the answer
if (isLeapYear) {
    console.log("Part 2: " + year + " is a leap year.");
} else {
    console.log("Part 2: " + year + " is NOT a leap year.");
}