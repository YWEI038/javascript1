"use strict";

// Provided variables.
var code = "M";

// Variables you'll be assigning to in this question.
var gender;

// TODO Your code for part (1) here.
if (code == 'M' || code == 'm') {
    gender ='Male';
} else  if (code == 'F' || code == 'f') {
    gender = 'Female';
} else  {
    gender = 'Unknown';
}


// Printing the answer
console.log("Part 1: The gender is: " + gender)