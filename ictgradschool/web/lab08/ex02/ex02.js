"use strict";

// Provided variables
var amount = 2;
var userResponse = 'N';
var firstName = "Ann";
var singer = "Taylor Swift";
var year = 2014;

// Example
var isOdd = (amount % 2 != 0);
console.log("isOdd = " + isOdd);
var response = (userResponse == 'Y');
console.log("userResponse = " + response);
var firstChar = (firstName[0] == 'A');
console.log("firstChar = " + firstChar);
var name = (singer == 'Taylor Swift');
console.log("singer = " + name);
var yearTest = (year > 1978 && year != 2013);
console.log("year = " + yearTest);
// TODO write your answers here
